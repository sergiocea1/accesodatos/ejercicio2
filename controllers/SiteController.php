<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionConsultadao(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT count(dorsal) AS Total FROM ciclista',
            'totalCount' => $numero,
            /*'pagination' =>[
                'pageSize' => 5,
                
            ]*/
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(dorsal) FROM ciclista",
        ]);
    }
    /**
     * Realizamos aquí las consultas con ORM
     */
    public function actionConsultaorm(){
        
        //Con ActiveRecord
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select('count("dorsal") dorsal'),
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(dorsal) FROM ciclista",
        ]);
    }
    /**
     * Realizamos aquí las consultas con DAO
     */
    public function actionConsultadao2(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT COUNT(dorsal) AS Total FROM ciclista WHERE nomequipo = "Banesto"',
            'totalCount' => $numero,
            /*'pagination' =>[
                'pageSize' => 5,
                
            ]*/
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    /**
     * Realizamos aquí las consultas con ORM
     */
    public function actionConsultaorm2(){
        
        //Con ActiveRecord
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('count("dorsal") dorsal')->distinct()->where("nomequipo = 'Banesto'"),
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT count(dorsal) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    public function actionConsultadao3(){
        $numero = Yii::$app->db
                ->createCommand('select avg(edad) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT AVG(edad) AS Media FROM ciclista',
            'totalCount' => $numero,
            /*'pagination' =>[
                    'pageSize'=>5
            ] */
        ]);
        
        return $this -> render("resultados",[
           "resultados"=>$dataProvider,
            "campos"=>['Media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista",
        ]);
    }
    public function actionConsultaorm3(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("AVG(edad) edad"),
            /*'pagination' =>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) FROM ciclista",
        ]);
    }
    public function actionConsultadao4(){
       //contar registros que devuelve la consulta
        $numero = Yii::$app->db
                ->createCommand('select avg(edad) from ciclista where nomequipo = "Banesto"')
                ->queryScalar();
        
        //creo el proveedor de datos con la consulta preparada
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT AVG(edad) AS Media FROM ciclista WHERE nomequipo = "Banesto"',
            'totalCount' => $numero,
            /*'pagination' => [
                'pageSize' => 5,
            ]*/
        ]);
        //renderizamos lla vista de la consulta
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=> ['Media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    }
    public function actionConsultaorm4(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select("AVG(edad) edad")->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    }
    
    public function actionConsultadao5(){
        $numero = Yii::$app->db
                ->createCommand('select count(nomequipo) from ciclista group by nomequipo')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT nomequipo,avg(edad) AS Media_Edad FROM ciclista GROUP BY nomequipo',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Media_Edad'],
            "titulo"=>"Consulta 5 de DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,avg(edad) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultaorm5(){
        $dataProvider = new ActiveDataProvider([
            'query' =>Ciclista::find()->select(['nomequipo','count("dorsal") dorsal'])->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>5
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','dorsal'],
            "titulo"=>"Consulta 5 de Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,avg(edad) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultadao6(){
        $numero = Yii::$app->db
                ->createCommand('select count(nomequipo) from ciclista group by nomequipo')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nomequipo,count(dorsal) AS Num_Ciclistas FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pagesize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'Num_Ciclistas'],
            "titulo"=>"Consulta 6 DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo,count(dorsal) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultaorm6(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select(['nomequipo','AVG(edad) edad'])->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo,count(dorsal) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultadao7(){
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            //declarar la variable $nombre_mayusculas en el modelo Ciclista, dentro de la clase
            'sql'=>'SELECT count(nompuerto) AS Puertos FROM puerto',
            'totalCount'=>$numero,
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT count(nompuerto) FROM puerto"
        ]);
                  
        }
    public function actionConsultaorm7(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('count("nompuerto") nompuerto'),
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT count(nompuerto) FROM puerto"
        ]);
    }
    public function actionConsultadao8(){
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto where altura >1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(nompuerto) AS Puertos FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>" El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) FROM puerto WHERE altura > 1500"
        ]);
                  
        }
    public function actionConsultaorm8(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select("count('nompuerto') nompuerto")->where("altura>1500"),
            /*'pagination'=>[
                'pageSize'=>5,
            ]*/
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>" El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) FROM puerto WHERE altura > 1500"
        ]);
    }
    public function actionConsultadao9(){
        $numero = Yii::$app->db
                ->createCommand('select count(nomequipo) from ciclista group by nomequipo having count(dorsal)>4')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ] 
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4"
        ]);
    }
    public function actionConsultaorm9(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('nomequipo')->groupBy('nomequipo')->having('count("dorsal")>4'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4"
        ]);
    }
    public function actionConsultadao10(){
        $numero = Yii::$app->db
                ->createCommand('select count(nomequipo) from ciclista where edad between 28 and 32 group by nomequipo having count(dorsal)>4')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4"
        ]);
    }
    public function actionConsultaorm10(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('nomequipo')->where("edad between 28 and 32")->groupBy('nomequipo')->having('count("dorsal")>4'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4"
        ]);
        
    }
    public function actionConsultadao11(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from etapa group by dorsal')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT dorsal, count(numetapa) AS "victorias" FROM etapa GROUP BY dorsal',
            'totalCount'=> $numero,
            /*'pagination'=>[
                'pageSize'=>5,
            ] */  
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','victorias'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, count(numetapa) FROM etapa GROUP BY dorsal"
        ]);
    }
    public function actionConsultaorm11(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Etapa::find()->select(['dorsal','count("numetapa") numetapa'])->groupBy('dorsal'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'numetapa'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, count(numetapa) FROM etapa GROUP BY dorsal"
        ]);
        
    }
    public function actionConsultadao12(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from etapa GROUP BY dorsal having count(numetapa)>1')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1"
        ]);
    }
    public function actionConsultaorm12(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Etapa::find()->select(['dorsal','count("numetapa") numetapa'])->groupBy('dorsal')->having('count("numetapa")>1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','numetapa'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1"
        ]);
        
    }
}